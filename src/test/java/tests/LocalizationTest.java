/**
 * This class tests the localization class language packs and if they actually exist and are in the right place.
 * 
 * @author Amir
 * 
 * 
 */
package tests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

	public class LocalizationTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}



	@Test
	public void fileTest3() throws IOException {
		Path path = Paths.get("src/main/resources/TextResources_en_GB.properties");
		assertAll(
		        () -> assertTrue("File should exist", Files.exists(path)));
		        	
	}
	
	@Test
	public void fileTest4() throws IOException {
		Path path = Paths.get("src/main/resources/TextResources_fi_FI.properties");
		assertAll(
		        () -> assertTrue("File should exist", Files.exists(path)));
		        	
	}
	
	

}

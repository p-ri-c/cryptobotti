/**
 * This tests the bitcoin class by using setters and getters.
 * 
 * @author Amir
 * 
 * 
 * 
 */

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import model.Bitcoin;

	public class BitcoinTest {
	
	 static Bitcoin btc = new Bitcoin();
	 
	 @BeforeAll
		static void setUpBeforeClass() throws Exception {
			btc.setO(12.5);
			btc.setC(0.11);
			btc.setH(0.50);
			btc.setL(0.32);
		}

	@Test
	public void btcTest1() {
		assertEquals( 12.5, btc.getO(),0.0);
		
	}
	
	@Test
	public void btcTest2() {
		assertEquals( 0.11, btc.getC(),0.0);
		
	}
	
	@Test
	public void btcTest3() {
		assertEquals( 0.50, btc.getH(),0.0);
		
	}
	
	
	@Test
	public void btcTest4() {
		assertEquals(0.32, btc.getL(),0.0);
		
	}

}
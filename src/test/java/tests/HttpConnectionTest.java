/**
 * Tests the HttpConnection class by setting a new endpoint
 *  and then using the getter to get it from the class.
 * 
 * @author Amir
 * 
 */

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import controller.HmacSHA256;
import controller.HttpConnection;

	public class HttpConnectionTest {

	 HmacSHA256 HmacSha = new HmacSHA256();
	
	 HttpConnection httpcon = new HttpConnection();
	
	@Test
	public void testEndpoint() {
		httpcon.setEndPoint("Asdasd");
		assertEquals( "Asdasd", httpcon.getEndPoint());
				
	}

}

/**
 * This class tests if the info class creates the file and that it exists in the right place and with the right name.
 * 
 * 
 * @author Amir
 * 
 */

package tests;


import org.junit.jupiter.api.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import model.Personalinfo;


	 public class PersonalinfoTest {

	 Personalinfo info = new Personalinfo();

	@Test
	public void fileTest() {
		info.CreateFile();
		assertEquals( "UserInfo", info.returnCreatedFile());
		
	}
	
	@Test
	public void fileTest2() throws IOException {
		Path path = Paths.get("UserInfo");
		assertAll(
		        () -> assertTrue("File should exist", Files.exists(path)));
		        	
	}
	

}
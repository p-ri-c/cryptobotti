package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Localization {
	private Properties catalog = new Properties();
	private String appConfigPath = "res/TextResources_en_GB.properties";
	private String catalogConfigPath = "res/Catalog.properties";
	private String language;
	private String country;

	public void onStart_GB() throws FileNotFoundException, IOException {

		// default kieli englanti
		Properties properties = new Properties();
		properties.load(new FileInputStream(appConfigPath));
		language = properties.getProperty("language");
		country = properties.getProperty("country");
		getCatalog().load(new FileInputStream(catalogConfigPath));
	}

	public Properties getCatalog() {
		return catalog;
	}

	public void setCatalog(Properties catalog) {
		this.catalog = catalog;
	}
}

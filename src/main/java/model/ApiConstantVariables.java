/**

 * This class contains the variables needed to use the API and getters and setters for each of them for their use.
 * 
 * @returns
 * apiRootURL = The url of the API root.
 * recv = How long will the API call will take for latency.
 * apiKey = the key needed to use the API.
 * secretKey = the other key that is specific for your user in the Binance Excahnge.
 * 
 * @author Valtteri
 * 
 */

package model;

public class ApiConstantVariables {

	private final String apiRootURL;
	private final String recv;
	private String secretKey;
	private String apiKey;

	public ApiConstantVariables() {
		this.apiRootURL = "https://api.binance.com";
		this.recv = "recvWindow=20000";

		// TODO: This needs to be done ones and then user
		// can just login and interface remembers api and secret
		this.secretKey = "fill your own info :D";
		this.apiKey = "fill your own info :D";
		// ^Fill here your own secretKey and apiKey
	}

	/**
	 * Returns apis root url
	 * @return apiRootURL String
	 */

	public String getApiRootURL() {
		return apiRootURL;
	}

	/**
	 * Returns recv paramaeter
	 * @return recv 
	 */

	public String getRecv() {
		return recv;
	}

	/**
	 * Returns api KEY which user sets
	 * @return apiKey
	 */

	public String getApiKey() {
		return apiKey;
	}

	/**
	 * Returns secret Key which user sets
	 * @return secretKey
	 */

	public String getSecretKey() {
		return secretKey;
	}

	/**
	 * Sets ApiKey
	 * @param
	 */

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * Sets SecretKey
	 * @param 
	 */
	
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

}

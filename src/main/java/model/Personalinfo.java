package model;
	/**
 * @author Amir
 * Two methods the first creates an empty file and the second writes inside the file the apikey and the secret key.
 * 
 */

	import java.io.File;  // Import the File class
	import java.io.FileOutputStream;
	import java.io.FileWriter;
	import java.io.IOException;  // Import the IOException class to handle errors
	import java.io.OutputStream;

	public class Personalinfo {
		ApiConstantVariables variables = new ApiConstantVariables();
		// Name of the file
		File file = new File("UserInfo");
		
	 /**
	 * This method creates a file if it alredy excists it will print out "File already exists."
	 * 
	 * 
	 * @throws IOException Signals that an I/O exception of some sort has occurred. 
	 */
	public void CreateFile() {
		
	      try {
			if (file.createNewFile()) {
			    System.out.println("File created: " + file.getName());
			  } else {
			    System.out.println("File already exists.");		
			  }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	/**
	 * This method writes the users apikey and secret key to the file when the user presses log in button in the UI.
	 * 
	 * 
	 * @param ApiKey Apikey from binance.
	 * @param SecretKey The users secretkey from binance.
	 */
	public void WriteTofile(String ApiKey, String SecretKey ) {
	
		String combined = ApiKey + "\n" + SecretKey;
	try {
		OutputStream out = new FileOutputStream(file);
		out.write(combined.getBytes(),0,combined.length());
		variables.setApiKey(ApiKey);
		variables.setSecretKey(SecretKey);
		
	} catch (Exception e) {
		System.out.print(e.getMessage());
		}
		
	}

	/**
	 * This method is used in tests it returns the name of the created file..
	 * 
	 * @return
	 * file.getName name of the created file.
	 */
	public Object returnCreatedFile() {
		return file.getName();
	}
	
	
}


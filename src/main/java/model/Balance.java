
/**

 * This class shows you how much you have money on your account how much is currently locked in assets.
 * 
 * @returns
 * asset= How much assets you have.
 *
 * free= How much money is not currently locked in assets
 * locked= How much money is currently locked in assets
 * 
 * @author Jaakko
 * 
 */


package model;

public class Balance {
	
	String asset;
	String free;
	String locked;
	
	public void setAsset(String asset) {
		this.asset = asset;
	}
	
	public void setFree(String free) {
		this.free = free;
	}
	
	public void setLocked(String locked) {
		this.locked = locked;
	}
	
	public String getAsset() {
		return asset;
	}
	
	public String getFree() {
		return free;
	}
	
	public String getLocked() {
		return locked;
	}
	
	@Override
	public String toString() {
		return "[" + this.asset + "] Free: (" + this.free + ") Locked: (" + this.locked + ")";
	}

}

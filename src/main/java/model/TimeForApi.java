/* 
 * This class is used to get api timestamp.
 * 
 * @author Valtteri
 *
 */


package model;

import java.sql.Timestamp;

public class TimeForApi {
	
	private Timestamp time;
	
	public TimeForApi() {
		
	}
	
	// Creates timestamp string and appends current time in ms
	public String getTimestamp() {
		time = new Timestamp(System.currentTimeMillis());
		String timestamp = "timestamp=";
		String t = String.valueOf(time.getTime() - 1000);
		
		return timestamp + t;
	}

}

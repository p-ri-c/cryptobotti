/**
 * This class represents the Bitcoin. It has all the necessary variables to hold the values needed to make investment decisions. 
 * 
 * 
 * @author Jan
 * @version 0.1
 * 
 */

package model;

public class Bitcoin {

	private String symbol; // Valuutan Tunnus
	private double o; // Open price
	private double c; // Closing price
	private double h; // Highest price
	private double l; // Lowest price

	public Bitcoin() {

	}

	/**
	 * The constructor for the Bitcoin
	 * 
	 * @param symbol The symbol used to read and store the correct currency
	 * @param o      The variable for opening price
	 * @param c      The variable for closing price
	 * @param h      The variable for highest price
	 * @param l      The variable for lowest price
	 */
	public Bitcoin(String symbol, double o, double c, double h, double l) {
		this.symbol = symbol;
		this.o = o;
		this.c = c;
		this.h = h;
		this.l = l;
	}

	/**
	 * The getter-method for the symbol
	 * 
	 * @return String containing the symbol of the bitcoin
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * The setter-method for the symbol
	 * 
	 * @param symbol The symbol used to read and store the correct currency
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * The getter-method for the opening price
	 * 
	 * @return o the double value of opening price
	 */
	public double getO() {
		return o;
	}

	/**
	 * The setter-method for the opening price
	 * 
	 * @param o double value of opening price
	 */
	public void setO(double o) {
		this.o = o;
	}

	/**
	 * The getter-method for the closing price
	 * 
	 * @return c the double value for closing price
	 */
	public double getC() {
		return c;
	}

	/**
	 * The setter-method for the closing price
	 * 
	 * @param c the double value for closing price
	 */
	public void setC(double c) {
		this.c = c;
	}

	/**
	 * The getter-method for the highest price
	 * 
	 * @return c the double value for highest price
	 */
	public double getH() {
		return h;
	}

	/**
	 * The setter-method for the highest price
	 * 
	 * @param h the double value for the highest price
	 */
	public void setH(double h) {
		this.h = h;
	}

	/**
	 * The getter-method for the lowest price
	 * 
	 * @return l the double value for lowest price
	 */
	public double getL() {
		return l;
	}

	/**
	 * The setter-method for the lowest price 
	 * @param l the double value for lowest price
	 */
	public void setL(double l) {
		this.l = l;
	}
	/**
	 * The toString-method for printing the values stored into the Bitcoin object
	 */
	@Override
	public String toString() {
		return "Crypto-coin " + symbol + " Opening price: " + o + " Closing price: " + c + " Highest price: " + h
				+ " Lowest price: " + l;

	}
}

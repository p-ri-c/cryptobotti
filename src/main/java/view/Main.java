package view;



/**
 * (https://stackoverflow.com/questions/52569724/javafx-11-create-a-jar-file-with-gradle/52571719#52571719)
 * This class is needed because the newest javafx is searching for a jar file but because were using a module
 * it cant find the packages and a wierd error comes.
 * 
 * @author Valtteri
 *
 */
public class Main {
	
public static void main(String[] args) {
		
		Login.main(args);

	}

}

/*
 * This class Handels actions that need to be done in login and loads user to info page and 
 * Gives the infopage a litle time to load so user wont see filler data
 * 
 * @param Actionevent
 * Javafx button 
 * @return Changes the scene to the frontpage from the login page
 * @author Valtteri
 * 
 * 
 */

package view;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import controller.LocaleResController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class FXMLLoginController {
	Stage stage;
	Parent root;
	private LocaleResController lc = new LocaleResController();
	private ResourceBundle bundleFI = ResourceBundle.getBundle(lc.getResFI());
	private ResourceBundle bundleEN = ResourceBundle.getBundle(lc.getResEN());

	@FXML
	private Button login;
	@FXML
	private Button fiengButton;
	
	String atThisMoment = "";

	/*
	 * Handels actions that need to be done in login and loads user to info page
	 * 
	 */

	@FXML
	public void initialize() {

		atThisMoment = lc.read();
		
	}

	@FXML
	protected void handleLoginButtonAction(ActionEvent event) throws IOException {

		// TODO: Lisää tarkistus if lauseeseen sisään kirjautumiseen että onko apikey ja
		// secret key oikein

		// success
		if (event.getSource() == login) {
			URL urlInfo = Paths.get("./src/main/java/view/infoPage.fxml").toUri().toURL();
			stage = (Stage) login.getScene().getWindow();
			if(lc.read().equals("EN")) {
				root = FXMLLoader.load(urlInfo, bundleEN);
			}else {
				root = FXMLLoader.load(urlInfo, bundleFI);
			}
			

			// fail
		} else {
			URL urlLogin = Paths.get("./src/main/java/view/loginPage.fxml").toUri().toURL();
			stage = (Stage) login.getScene().getWindow();
			//root = FXMLLoader.load(urlLogin, bundleEN);
			if(lc.read().equals("EN")) {
				root = FXMLLoader.load(urlLogin, bundleEN);
			}else {
				root = FXMLLoader.load(urlLogin, bundleFI);
			}
		}

		// Giving the infopage a litle time to load so user wont see filler data
		// TODO: Could make this better? Or do we need this at all?
		try {
			TimeUnit.SECONDS.sleep(2);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Setting the stage depending on the input
		Scene scene = new Scene(root);
		stage.setScene(scene);
		stage.show();
		stage.setTitle(bundleEN.getString("frontpage"));

	}

	@FXML
	private void handleToggleButtonPress(ActionEvent event) throws IOException {
		
		
		
		if (lc.read().equals("EN")) {
			//System.out.println(lc.read() + " 1");
			lc.write("FI");
			URL urlLogin = Paths.get("./src/main/java/view/loginPage.fxml").toUri().toURL();
			stage = (Stage) login.getScene().getWindow();
			root = FXMLLoader.load(urlLogin, bundleEN);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
			stage.setTitle(bundleEN.getString("frontpage"));
			
		}else {
			//System.out.println(lc.read() + " 2");
			lc.write("EN");
			URL urlLogin = Paths.get("./src/main/java/view/loginPage.fxml").toUri().toURL();
			stage = (Stage) login.getScene().getWindow();
			root = FXMLLoader.load(urlLogin, bundleFI);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
			stage.setTitle(bundleFI.getString("frontpage"));
		}
		
		
	}

}

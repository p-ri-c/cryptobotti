/*
 * This class changes the language in the scene from the TextResources_en_GB file.
 * @author Jan
 * 
 * 
 * @throws IOException
 * Signals that an I/O exception of some sort has occurred.
 *  Thisclass is the general class of exceptions produced by failed orinterrupted I/O operations.
 * 
 */


package view;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.ResourceBundle;

import controller.LocaleResController;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.fxml.*;

public class Login extends Application {

	@Override
	public void start(Stage stage) throws IOException {
		LocaleResController lc = new LocaleResController();
			
		ResourceBundle bundle = ResourceBundle.getBundle(lc.getLocaleRes(), lc.getLocale());

		// First we get the fxml that we need
		URL url = Paths.get("./src/main/java/view/loginPage.fxml").toUri().toURL();
		Parent root = FXMLLoader.load(url, bundle);

		Scene scene = new Scene(root, 640, 480);
		stage.setTitle(bundle.getString("loginpagetitle"));
		stage.setScene(scene);

		stage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

}
/*
 * 
 * This class handles the diffrent buttons on the mainpage
 * 
 * 
 * @param Actionevent
 * Javafx button 
 * @return Changes the scene according to the users button press
 * @author Valtteri
 * 
 */

package view;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;

import controller.LocaleResController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class FXMLPageSwitchController {
	
	@FXML private Button infoPageButton;
	@FXML private Button botPageButton;
	private LocaleResController lc = new LocaleResController();
	private ResourceBundle bundle = ResourceBundle.getBundle(lc.getLocaleRes(), lc.getLocale());
	
	@FXML protected void handelInfoButtonAction(ActionEvent event) throws IOException {
		
		Stage stage;
		Parent root;
		
		if (event.getSource()==infoPageButton) {
			URL urlInfo = Paths.get("./src/main/java/view/infoPage.fxml").toUri().toURL();
			stage = (Stage) infoPageButton.getScene().getWindow();
			root = FXMLLoader.load(urlInfo, bundle);
			
			Scene scene = new Scene(root);
			stage.setTitle(bundle.getString("frontpage"));
			stage.setScene(scene);
			stage.show();
		}
	}
	
	@FXML protected void handelBotButtonAction(ActionEvent event) throws IOException {
		
		Stage stage;
		Parent root;
		
		if (event.getSource()==botPageButton) {
			URL urlBot = Paths.get("./src/main/java/view/botPage.fxml").toUri().toURL();
			stage = (Stage) botPageButton.getScene().getWindow();
			root = FXMLLoader.load(urlBot, bundle);
			
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle(bundle.getString("botPage"));
			stage.show();
		}
	}

}

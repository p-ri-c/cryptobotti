/**
 * This class aims to manage data to the right format for database insertion.
 * 
 * 
 * @param Session A Web Socket session represents a conversation between two web socket endpoints. 
 * 
 * @return 
 * 
 * @throws IOException Signals that an I/O exception of some sort has occurred.
 *  Thisclass is the general class of exceptions produced by failed orinterrupted I/O operations.
 * @author Valtteri
 * 
 */

package controller;

import java.io.IOException;
import java.util.logging.Logger;

import javax.sound.sampled.SourceDataLine;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

//Luokka raa'an datan muokkaamiseen sopivaan muotoon tietokantaa varten
@ServerEndpoint("/bitcoin")
public class DataCleaner{
    private Session session;
    @OnOpen
    public void onOpen(Session session) throws IOException {
        this.session = session;
    }

    @OnMessage
    public void onMessage(Session session, String message) throws IOException {
        System.out.println("Dataa tulee ="+ message);
        try{
        if(this.session != null && this.session.isOpen()){
            this.session.getBasicRemote().sendText("From server "+message);
        }}catch(IOException exception){
            System.out.println("Errorii tulloo");
        }
    }

    @OnClose
    public void onClose(Session session) throws IOException {
        // WebSocket connection closes
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        // Do error handling here
    }
    
}

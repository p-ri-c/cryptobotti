/**
 * This class is for future development and isn't yet working as intented.
 * 
 * This class contains the strategy which the bot uses to decide to buy or sell. It uses the data that is 
 * stored in the database Openingprice,Closingprice. 
 * 
 * 
 * 
 * @author Jaakko
 */

package controller;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.ZonedDateTime;

import org.apache.logging.log4j.core.config.Scheduled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.ta4j.core.Bar;
import org.ta4j.core.BaseBar;
import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.helpers.DifferenceIndicator;
import org.ta4j.core.num.Num;

import model.Bitcoin;


public class Strategy {
	private static final Logger logger = LoggerFactory.getLogger(Strategy.class);
	
	private String symbol;
	private double quantity;
	Bitcoin btc = new Bitcoin();
	BaseTimeSeries series;

	private int emaPeriodShort;

	private int emaPeriodLong;

	private DifferenceIndicator emaDifference;
	
	public Strategy() {
		this.series = new BaseTimeSeries();
	    }
	
    


    public void setProperties() {
    	ClosePriceIndicator closePriceIndicator = new ClosePriceIndicator(this.series);
    	EMAIndicator emaShort = new EMAIndicator(closePriceIndicator, this.emaPeriodShort);
        EMAIndicator emaLong = new EMAIndicator(closePriceIndicator, this.emaPeriodLong);
        this.emaDifference = new DifferenceIndicator(emaShort, emaLong);
    }
    
    
//    Antaa käskyn suorittaa osto   
//    public void handleTradeEvent() {
//
//        if (this.series.getEndIndex() >= 0) {
//            synchronized (series) {
//                series.addTrade(Math.abs(event.getAmount()), event.getPrice());
//            }
//        }
//    }
//    
//    
//    @Scheduled(cron = "*/10 * * * * *")
//    public void onTime() {
//        synchronized (series) {
//            try {
//                logBar();
//                evaluateLogic();
//                createNewBar();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//		
//	75-131 rivit odottelee ostotapahtuman luomista ja muokkaa sitten kuntoon
//    private void logBar() {
//        
//        int i = this.series.getEndIndex();
//        if (i > 0 && i < emaPeriodLong) {
//            Bar bar = this.series.getBar(i);
//           
//            logger.info("open {} high {} low {} close {} vol {} trades {}",
//                    bar.getOpenPrice(),
//                    bar.getMaxPrice(),
//                    bar.getMinPrice(),
//                    bar.getClosePrice(),
//                    bar.getVolume(),
//                    bar.getTrades());
//            
//        } else if (i >= emaPeriodLong) {
//
//            Bar bar = this.series.getBar(i);
//            Num emaDiff = this.emaDifference.getValue(i);
//            Num emaDiffPrev = this.emaDifference.getValue(i - 1);
//
//            logger.info("open {} high {} low {} close {} vol {} trades {} emaDiffPrev {} emaDiff {}",
//                    bar.getOpenPrice(),
//                    bar.getMaxPrice(),
//                    bar.getMinPrice(),
//                    bar.getClosePrice(),
//                    bar.getVolume(),
//                    bar.getTrades(),
//                    emaDiffPrev,
//                    emaDiff);
//        }
//    }
//    
//    
//        private void evaluateLogic() {
//
//        	int i = this.series.getEndIndex();
//        	if (i >= emaPeriodLong) {
//
//        		Num emaDiff = this.emaDifference.getValue(i);
//        		Num emaDiffPrev = this.emaDifference.getValue(i - 1);
//
//            
//        		if (emaDiff.doubleValue() > 0 && emaDiffPrev.doubleValue() <= 0) {
//
//        			logger.info("!!!!!!!! BUY !!!!!!!!!)");
//        			tradingService.sendOrder("buy", quantity, symbol);
//
//        		} else if (emaDiff.doubleValue() < 0 && emaDiffPrev.doubleValue() >= 0) {
//
//        			logger.info("!!!!!!!! SELL !!!!!!!!!");
//        			tradingService.sendOrder("sell", quantity, symbol);
//        		}
//        	}
//        
//        
//        
//    }
//		Päivittää bitcoinin hintaa 6s välein
        private void createNewBar() {
            
            // create new bar
            ZonedDateTime now = ZonedDateTime.now();
            Duration duration = Duration.ofSeconds(6);
            Bar newBar = new BaseBar(duration, now, this.series.function());
        
            // set price to closing price of previous bar
            int i = this.series.getEndIndex();
            if (i >= 0) {
                Bar previousBar = series.getBar(i);
                newBar.addPrice(previousBar.getClosePrice());
            }
        
            series.addBar(newBar);
        }
	
	

	


    

    
    
	
	
	
	

}

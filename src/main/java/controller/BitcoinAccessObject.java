/**
 * 
 * This class acts as a dataAccesObject, connects to the database and inserts bitcoin values to the database table.
 * It also reads the values from the database.
 * @author Jan
 * @version 0.1
 */


package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Bitcoin;

public class BitcoinAccessObject implements IBitcoinDAO {
	private Connection conn;
	private Statement stmt;
	private Bitcoin bitcoin = null;
	// boolean hasBitcoin = false;

	static final String JBCD_DRIVER = "org.mariadb.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://10.114.32.76/3306";

	// Logging into the database

	static final String USER = "kayttaja";
	static final String passw = "kayttaja";

	// Table name in the database: Bitcoin_table
	// varchar symbol(tunnus), double o(open price), double c(Close price), h
	// double(highest price), l double (lowest price),
	// n int (number of trades)

	
	/**
	 * Method for testing the connectivity to the database
	 * @throws SQLException
	 */
	public void avaaSqlYhteys() {

		try {
			// JCBD driver installation
			Class.forName("org.mariadb.jdbc.Driver");

			// Open connection
			System.out.println("Otetaan yhteyttä tietokantaan...");
			conn = DriverManager.getConnection("jdbc:mariadb://10.114.32.76:3306/cryptoDB", "kayttaja", "kayttaja");
			System.out.println("Yhdistetty tietokantaan onnistuneesti...");

			// test to set v

		} catch (SQLException se) {
			// handle JDBC errors
			se.printStackTrace();
		} catch (Exception e) {
			// handle Class.forName
			e.printStackTrace();
		}
	}

	/**
	 * Method for adding the bitcoin's values to the database
	 * @param Bitcoin  	
	 * 			the bitcoin-object that holds the values for the currency
	 * @return boolean 
	 * 		== true if the bitcoin currency has been succesfully added to the database
	 */
	public boolean createBitcoin(Bitcoin bitcoin) {
		// create statement			
		try (PreparedStatement createStatement = conn
				.prepareStatement("INSERT INTO bitcoin_table(symbol, o, c, h, l) VALUES (?, ?, ?, ?, ?);")) {
			
			// Set the values			
			createStatement.setString(1, bitcoin.getSymbol());
			createStatement.setDouble(2, bitcoin.getO());
			createStatement.setDouble(3, bitcoin.getC());
			createStatement.setDouble(4, bitcoin.getH());
			createStatement.setDouble(5, bitcoin.getL());
			
			//Send statement			
			createStatement.executeUpdate();

			System.out.println("Lisätty onnistuneesti.");
			return true;
			//handle errors 			
		} catch (SQLException sqle) {
			do {
				System.err.println("Datan luominen epäonnistui.");
				sqle.printStackTrace();
			} while (sqle.getNextException() != null);

			return false;
		}
	}

	/**
	 * This method reads bitcoin values from the database and returns a bitcoin object that holds those values
	 * @return Bitcoin 
	 * 			if the query was succesfull
	 * @exception SQLException 
	 * 				is given if the query fails
	 */
	public Bitcoin readBitcoin() {

		try (PreparedStatement readBitcoinStatement = conn
				.prepareStatement("SELECT * FROM bitcoin_table WHERE symbol = ?;")) {

			readBitcoinStatement.setString(1, "btc");
			ResultSet rSet = readBitcoinStatement.executeQuery();

			if (rSet.next()) {
				String rsSymbol = rSet.getString("symbol");
				double rsOpenPrice = rSet.getDouble("o");
				double rsClosingPrice = rSet.getDouble("c");
				double rsHighestPrice = rSet.getDouble("h");
				double rsLowestPrice = rSet.getDouble("l");
				bitcoin = new Bitcoin(rsSymbol, rsOpenPrice, rsClosingPrice, rsHighestPrice, rsLowestPrice);

			} else {
				return null;
			}

		} catch (SQLException sqle) {
			do {
				System.err.println("Datan hakeminen ep�onnistui.");
				sqle.printStackTrace();
			} while (sqle.getNextException() != null);
		}
		return bitcoin;
	}

//	public boolean updateBitcoin(Bitcoin bitcoin) {
//		try (PreparedStatement updateStatement = conn.prepareStatement(
//				"UPDATE bitcoin_table SET o = ?, SET c = ?, SET h = ?, SET l = ? WHERE symbol = btc;")) {
//
//			updateStatement.setString(1, bitcoin.getSymbol());
//			updateStatement.setDouble(2, bitcoin.getO());
//			updateStatement.setDouble(3, bitcoin.getC());
//			updateStatement.setDouble(4, bitcoin.getH());
//			updateStatement.setDouble(5, bitcoin.getL());
//
//			updateStatement.executeUpdate();
//
//			return true;
//
//		} catch (SQLException sqle) {
//			do {
//				System.err.println("Datan päivittäminen epäonnistui.");
//				sqle.printStackTrace();
//			} while (sqle.getNextException() != null);
//		} catch (Exception e) {
//			System.err.println("Virhe.");
//			e.printStackTrace();
//		}
//		return false;
//	}
}

package controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.ApiConstantVariables;
import model.TimeForApi;

/**
 * !! endpoint string needs to be set before use !! Check
 * {@link #setEndPoint(String)}}
 * 
 * This class is the main class for getting data from the bitcoin api Only
 * parameter that needs to be used in this class is @endPoint it defines from
 * what address the class gets the json data
 * 
 * @author Valtteri
 */

public class HttpConnection {

	private final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
	HttpRequest req;

	ApiConstantVariables apiConstants = new ApiConstantVariables();
	TimeForApi timeForApi = new TimeForApi();
	HmacSHA256 hmac = new HmacSHA256();

	String endPoint = "";

	/**
	 * Creates private account queryString that is needed for the private request
	 * from binances api @return queryString
	 */

	private String privateUrlString(String extraValues) {

		// TODO: Check at the start that the endpoint is set

		String theURL = "";

		// Root url
		String root = apiConstants.getApiRootURL();

		// This 2 form the totalparameter for the signature calc
		String recv = apiConstants.getRecv();
		String timeStamp = timeForApi.getTimestamp();

		String totalParams = recv + "&" + timeStamp;

		// Calculating the signature and casting it to string (HmacSHA256 encryption)
		byte[] signatureBytes = hmac.calcHmacSha256ForApi(apiConstants.getSecretKey().getBytes(),
				totalParams.getBytes());
		String signature = String.format("&signature=%032x", new BigInteger(1, signatureBytes));

		// Finaly putting it all to getter to form the url
		theURL = root + this.endPoint + "?" + extraValues + totalParams + signature;

		return theURL;
	}
	
	/**
	 * Returns the signature needed for api requests
	 * @param timeStamp is given for the signature to add it to the string
	 */

	private String getSignature(String timeStamp) {
		String signature = "";

		String recv = apiConstants.getRecv();
		// String timeStamp = timeForApi.getTimestamp();
		System.out.println(timeStamp + "in Signature");
		String totalParams = recv + "&" + timeStamp;

		// Calculating the signature and casting it to string (HmacSHA256 encryption)
		byte[] signatureBytes = hmac.calcHmacSha256ForApi(apiConstants.getSecretKey().getBytes(),
				totalParams.getBytes());
		signature = String.format("&signature=%032x", new BigInteger(1, signatureBytes));

		return signature;
	}

	/**
	 * Creates public queryString that is needed for the basic request from binances
	 * api @return queryString
	 */

	private String publicUrlString(String extraValues) {

		String publicUrlString = "";

		publicUrlString += apiConstants.getApiRootURL();
		publicUrlString += this.endPoint;
		publicUrlString += extraValues;

		return publicUrlString;
	}

	/**
	 * Makes asyncronous request to binances api and return Json in String format
	 * This query is for getting private data from personal account
	 * 
	 * @return JsonString
	 */

	public String sendPrivateHttpRequestToApi() {

		String asyncResultBody = null;
		CompletableFuture<HttpResponse<String>> asyncResponce = null;

		req = HttpRequest.newBuilder(URI.create(this.privateUrlString("")))
				.header("X-MBX-APIKEY", apiConstants.getApiKey()).GET().build();

		asyncResponce = httpClient.sendAsync(req, HttpResponse.BodyHandlers.ofString());

		try {
			asyncResultBody = asyncResponce.thenApply(HttpResponse::body).get(5, TimeUnit.SECONDS);

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}

		return asyncResultBody;
	}

	/**
	 * Makes asyncronous request to binances api and return Json in String format
	 * This query is for getting public data
	 * 
	 * @return JsonString
	 */

	public String sendPublicHttpRequestToApi() {

		String asyncResultBody = null;
		CompletableFuture<HttpResponse<String>> asyncResponce = null;

		req = HttpRequest.newBuilder(URI.create(this.publicUrlString("")))
				.header("X-MBX-APIKEY", apiConstants.getApiKey()).GET().build();

		asyncResponce = httpClient.sendAsync(req, HttpResponse.BodyHandlers.ofString());

		try {
			asyncResultBody = asyncResponce.thenApply(HttpResponse::body).get(5, TimeUnit.SECONDS);

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}

		return asyncResultBody;
	}

	/**
	 * Sets the end point url where you wont to get the data from you need to just
	 * write for example( /api/v3/time or /api/v3/exchangeInfo )
	 */

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	/**
	 * Gets the endpoint url that is currently set
	 * @return endPoint that is set
	 */

	public String getEndPoint() {
		return endPoint;
	}

	/**
	 * Gets 3 parameter that the user defines and send that information to the api
	 * returns Json in a string format
	 * 
	 * @throws InterruptedException
	 * @throws IOException
	 */

	public String newOrder(String symbol, String side, double quantity, double price) throws IOException, InterruptedException {

		// This is needed for quantity and price conversion if the valu has big decimals
		BigDecimal quantityToDecimal = BigDecimal.valueOf(quantity);
		BigDecimal priceToDecimal = BigDecimal.valueOf(price);

		// Creating a linkedHashMap to keep the data in easy to format mode
		Map<Object, Object> data = new LinkedHashMap<>();
		data.put("symbol", symbol);
		data.put("side", side);
		data.put("type", "LIMIT");
		data.put("timeInForce", "GTC");
		data.put("quantity", quantityToDecimal.toString());
		data.put("price", priceToDecimal.toString());
		data.put("recvWindow", "20000");
		// TODO Make a glonbal time cheker!
		Timestamp time = new Timestamp(System.currentTimeMillis());
		String t = String.valueOf(time.getTime() - 1000);
		data.put("timestamp", t);

		// Total parameters are needed for signature calculation
		String totalParams = "symbol=BTCUSDT&side=" + side + "&type=LIMIT&timeInForce=GTC&quantity=" + quantityToDecimal
				+ "&price=" + priceToDecimal + "&recvWindow=20000&timestamp=" + t;

		// Calculating the signature
		String signature = "";
		byte[] hmacSha256 = hmac.calcHmacSha256ForApi(apiConstants.getSecretKey().getBytes(), totalParams.getBytes());
		signature += String.format("%032x", new BigInteger(1, hmacSha256));
		System.out.println("This is the shit that dosent work! " + signature);
		data.put("signature", signature);

		// Making the request body tobe send to the api
		// POST contains important method buildFormDataFromMap read doc.
		HttpRequest request = HttpRequest.newBuilder().POST(buildFormDataFromMap(data))
				.uri(URI.create(apiConstants.getApiRootURL() + getEndPoint()))
				.setHeader("User-Agent", "Cryptobotti api request") // add request header
				.header("X-MBX-APIKEY", apiConstants.getApiKey()).build();

		// Sending the request body to the api and storing it to httpResponce
		HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

		// Return the responce body as a string
		return response.body();
	}

	/**
	 * This method is from
	 * <p>
	 * <a>https://mkyong.com/java/how-to-send-http-request-getpost-in-java/</a>
	 * </p>
	 * It returns a BodyPublisher Object that contains the string reprensentation of
	 * the data that is going tobe send to the api as in key=value&key=value etc..
	 */

	private static HttpRequest.BodyPublisher buildFormDataFromMap(Map<Object, Object> data) {
		var builder = new StringBuilder();
		for (Map.Entry<Object, Object> entry : data.entrySet()) {
			if (builder.length() > 0) {
				builder.append("&");
			}
			builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
			builder.append("=");
			builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
		}
		System.out.println(builder.toString());
		return HttpRequest.BodyPublishers.ofString(builder.toString());
	}
	
	
	/**
	 * Gets Candelstick data from api and return string 
	 * */
	public String klines() {
		this.endPoint = "/api/v3/klines?symbol=BTCUSDT&interval=1m&limit=1";
		return this.sendPublicHttpRequestToApi();
	}

	// THIS IS JUST GARPAGE TEST THAT WILL BE REMOVED IN A FINAL
	// VERSION!-------------------------------------

	public static void main(String[] args) throws IOException, InterruptedException {

		HttpConnection hc = new HttpConnection();
		//hc.setEndPoint("/api/v3/order/test");
		// hc.setEndPoint("/api/v3/time");
		//String data = hc.newOrder("BTCUSDT", "BUY", 0.0006, 19444.40);
		// String data = hc.sendPublicHttpRequestToApi();
		double a = 19030.0100;
		double profit = 0;
		while(true) {
		String data = hc.klines();
		//System.out.println(data);
		
		int i = 0;
		
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonRootNode = objectMapper.readTree(data);
		//System.out.println(jsonRootNode.toPrettyString());
		//System.out.println(jsonRootNode.path(0).path(2));
		
		JsonNode openJ = jsonRootNode.path(0).path(1);
		JsonNode highJ = jsonRootNode.path(0).path(2);
		JsonNode lowJ = jsonRootNode.path(0).path(3);
		JsonNode closeJ = jsonRootNode.path(0).path(4);
		JsonNode volumeJ = jsonRootNode.path(0).path(5);
		JsonNode numOfTradesJ = jsonRootNode.path(0).path(8 );
		
		String openS = openJ.toString();
		String highS = highJ.toString();
		String lowS = lowJ.toString();
		String closeS = closeJ.toString();
		String volumeS = volumeJ.toString();
		String numOfTradesS = numOfTradesJ.toString();
		
		String openStriped = openS.replace("\"", "");
		String highStriped = highS.replace("\"", "");
		String lowStriped = lowS.replace("\"", "");
		String volumeStriped = volumeS.replace("\"", "");
		String closeStriped = closeS.replace("\"", "");
		String numOfTradesStriped = numOfTradesS.replace("\"", "");
		
		BigDecimal open = new BigDecimal(openStriped);
		BigDecimal high = new BigDecimal(highStriped);
		BigDecimal low = new BigDecimal(lowStriped);
		BigDecimal volume = new BigDecimal(volumeStriped);
		BigDecimal numOfTrades = new BigDecimal(numOfTradesStriped);
		BigDecimal close = new BigDecimal(closeStriped);
		
		if(i % 2 == 0) {
			profit += (high.doubleValue() - open.doubleValue()) * 0.001;
		}
		
		System.out.println("open_ ["+open+"]" + " high_ ["+high+"]" + " low_ ["+low+"]" + " num_of_trades_ ["+numOfTrades+"]" + " volume_ [" + volume + "]");
		System.out.println("profit_calc_ [" + profit + "]");
		System.out.println();
		//double d = Double.valueOf(b);
		//System.out.println(d);
		i++;
		Thread.sleep(8000);
		}
		/*
		 * // Aja tämä tiedosto jos haluat kokeilla connection luokan toimivuutta
		 * 
		 * // Luodaan connection olio HttpConnection hc = new HttpConnection();
		 * 
		 * // Annetaan sille oisoite mistä hakea dataa // HUOM! TÄMÄ ON SE MITÄ
		 * VAIHDETAAN hc.setEndPoint("/api/v3/ping");
		 * 
		 * // Haetaan data ja asetetaan se muuttujaan String data =
		 * hc.sendHttpRequestToApi();
		 * 
		 * // Koska data on json muodossa käsitellään sitä Jackson kirjastolla
		 * ObjectMapper objectMapper = new ObjectMapper(); JsonNode jsonRootNode =
		 * objectMapper.readTree(data);
		 * 
		 * // Tälleen pääsee käsiks sisempii nodeihin // Voi myös iteroida näiden läpi
		 * tai tehdä java objecteja niistä //JsonNode balances =
		 * jsonRootNode.at("/balances");
		 * 
		 * // Myös "kaunis" printtaus onnistuu
		 * //System.out.println(balances.toPrettyString());
		 * System.out.println(jsonRootNode.toPrettyString());
		 */
	}

}

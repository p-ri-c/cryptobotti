package controller;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MariaDB {
	// jbcd driveri ja databasen URL
	   static final String JBCD_DRIVER = "org.mariadb.jdbc.Driver";
	   static final String DB_URL = "jdbc:mysql://10.114.32.76/3306";
	   
	   // Databasen kirjautuminen
	   
	   static final String USER = "kayttaja";
	   static final String passw = "kayttaja";
	   
	   public void avaaSqlYhteys () 
	    {
	       Connection conn = null;
	       Statement stmt = null;
	   
	   try {
		   // JCBD ajurin asentaminen
		   Class.forName("org.mariadb.jdbc.Driver");
		   
		   // Avaa yhteys
		   System.out.println("Otetaan yhteyttä tietokantaan...");
	       conn = DriverManager.getConnection(
	               "jdbc:mariadb://10.114.32.76:3306/cryptoDB", "kayttaja", "kayttaja");
	       System.out.println("Yhdistetty onnistuneesti...");
	       
	       //kantaan tiedon asettamistesti
	        

	   } catch (SQLException se) {
	       //Käsittele JDBC virheet
	       se.printStackTrace();
	   } catch (Exception e) {
	       //Käsittele virheet Class.forName
	       e.printStackTrace();
	   } finally {
	       try {
	           if (stmt != null) {
	               conn.close();
	           }
	       } catch (SQLException se) {
	       }
	       try {
	           if (conn != null) {
	               conn.close();
	           }
	       } catch (SQLException se) {
	           se.printStackTrace();
	       }
	   }

	   }
	   
}
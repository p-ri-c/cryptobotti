/**
 * This class gets the data from the API for creating the strategy behind the bot. It converts JSON to bigdecimal.
 * 
 * 
 * 
 * @author Jaakko
 */

package controller;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TradeEvent {

    private BigDecimal lowPrice;
    private BigDecimal highPrice;
    private BigDecimal openPrice;
    private BigDecimal closePrice;
    private static HttpConnection connection;

    private TradeEvent(){
		
    	
    }

    /**
     * This method gets the data from the websocket as Json and turns it into bigdecimal.
     * 
     * 
     * @throws IOException
     * @throws Exception
     */
    
	public void fromJson() throws IOException, Exception {
    	connection = new HttpConnection();
        TradeEvent tradeEvent = new TradeEvent();
        String kline = connection.klines();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode json = objectMapper.readTree(kline);
        
		JsonNode open = json.path(0).path(1);
		String o = open.toPrettyString();
		String oR = o.replace("\"", "");
		System.out.println(oR);		
		BigDecimal oP = new BigDecimal(oR);
		
		JsonNode close = json.path(0).path(4);
		String c = close.toString();
		String oC = c.replace("\"", "");
		BigDecimal cP = new BigDecimal(oC);
		
		JsonNode high = json.path(0).path(2);
		String h = high.toString();
		String hR = h.replace("\"", "");
		BigDecimal hP = new BigDecimal(hR);
		
		
		JsonNode low = json.path(0).path(3);
		String l = low.toString();
		String lR = l.replace("\"", "");
		BigDecimal lP = new BigDecimal(lR);
		
		this.lowPrice = lP;
		this.highPrice = hP;
		this.closePrice = cP;
		this.openPrice = oP;
        
    }
    /**
	 * 
	 * @return lowPrice lowest price where the crypto asset has gone in the last 1 minute.
	 */
    public BigDecimal lowPrice() {
        return lowPrice;
    }
    /**
     * 
     * 
     * @return highPrice highest price where the crypto asset has gone in the last 1 minute.
     */
    public BigDecimal highPrice() {
        return highPrice;
    }
    /**
     * 
     * 
     * @return openPrice the starting price of the currency.
     */
    public BigDecimal getopenPrice() {
        return openPrice;
    }
    /**
     *  
     *  
     * @return closePrice The price of the currency at the end of the 1 minute window.
     */
    public BigDecimal getclosePrice() {
        return closePrice;
    }
    
    /**
     * 
     * @return the prices in String format.
     */
    @Override
    public String toString() {
        return "TradeEvent [lowprice=" + lowPrice + ", highprice=" + highPrice + ", openprice=" + openPrice + ", closeprice=" + closePrice + "]";
    }
    /**
     * This main is for testing.
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
    	TradeEvent lol = new TradeEvent();
    	lol.fromJson();
    	System.out.println(lol.toString());
    	
    }
}
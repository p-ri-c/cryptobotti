/**

 * This class manages diffrent locales and the resources needed for the corresponding languages.
 * 
 * 
 * 
 * @author Jan
 * @version 0.1
 */

package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.Scanner;

public class LocaleResController {

	private Locale locale = Locale.getDefault();
	private String resFI = "TextResources_fi_FI";
	private String resEN = "TextResources_en_GB";

	// for saving
	private String directory = System.getProperty("user.dir");
	private String fileName = "sample.txt";
	private String absolutePath = directory + "\\" + fileName;

	public LocaleResController() {
		// locale = Locale.getDefault();
	}

	public Locale getLocale() {
		return locale;
	}

	/**
	 * This method is responsible for returning the correct language resources.
	 * 
	 * @return ResourceFile for the matching language
	 */
	public String getLocaleRes() {

		if (this.read().equals("FI")) {
			return resFI;
		} else {
			return resEN;
		}
	}

	/**
	 * The getter-method for the english resource file.
	 * 
	 * @return String resEn holds the path for this resourcefile
	 */
	public String getResEN() {
		return resEN;
	}

	/**
	 * The getter-method for the finnish resource file.
	 * 
	 * @return String resFI holds the path for this resourcefile
	 */
	public String getResFI() {
		return resFI;
	}

	/**
	 * This method writes the language selection to a file, so that the chosen
	 * language persists even after restart.
	 * 
	 * @param s The String to be written to the file.
	 * @throws IOException handles the exceptions produced by failed or interrupted
	 *                     I/O operations.
	 */
	public void write(String s) {
		try {
			FileWriter myWriter = new FileWriter(absolutePath);
			myWriter.write(s);
			myWriter.close();
			System.out.println(s + " Successfully wrote to the file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	/**
	 * This method is responsible for reading the language resource file.
	 * 
	 * @return String that holds the correct language.
	 * @throws This exception will be thrown by the FileInputStream,
	 *              FileOutputStream, and RandomAccessFile constructors when a file
	 *              with the specified pathname does not exist. It will also be
	 *              thrown by these constructors if the file does exist but for some
	 *              reason is inaccessible, for example when an attempt is made to
	 *              open a read-only file for writing.
	 */
	public String read() {

		String s = "";

		try {
			File myObj = new File(absolutePath);
			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) {
				s = myReader.nextLine();
			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

		return s;
	}

}

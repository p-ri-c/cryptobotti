/**
 * @author Jani
 * 
 *This class is an interface for the Bitcoin dataAccessObject
 */

package controller;

import model.Bitcoin;

public interface IBitcoinDAO {
	public void avaaSqlYhteys();
	public boolean createBitcoin(Bitcoin bitcoin);
	public Bitcoin readBitcoin();
}

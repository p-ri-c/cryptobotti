/**
 * This class is for evaluating the market and making the buying decisions based on the current strategy.
 * 
 * @author Jaakko
 * 
 */
package controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;

import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.num.Num;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.stream.JsonReader;

import controller.TradeEvent;
public class TradingStrategy {

/** 	Mandatory parameters	*/
	static TradeEvent info = new TradeEvent();
	private BigDecimal openPrice;
	private BigDecimal closePrice;
	private BigDecimal difference;
	HttpConnection order = new HttpConnection();
	
	/**	BigDecimal deduction to get the difference	*/
	/**
	 * This method counts the diffrence between openprice and closing price.
	 * 
	 * @throws IOException
	 * @throws Exception
	 * @return the diffrence.
	 */	
	
	public void bgDeduct() throws IOException, Exception {
		info.fromJson();
		MathContext mc = new MathContext(6);
		BigDecimal bg1 = info.getclosePrice();
		BigDecimal bg2 = info.getopenPrice();
		BigDecimal bg3 = bg1.subtract(bg2, mc); 
		
		this.difference = bg3;
		
	}

	/**	Simple trading strategy to use	*/
	
	/**
	 * This method evaluates the market and makes the order if it hits certain criteria.
	 * 
	 * @throws IOException
	 * @throws Exception
	 * @return The buy order to the API.
	 */
	
	public void strategy() throws IOException, Exception {
		bgDeduct();
		BigDecimal lower = new BigDecimal(1);
		if(this.difference.doubleValue() > 10) {
			System.out.println("ORDER TEHTY");
			order.setEndPoint("/api/v3/order/test");
	/**			order.newOrder("BTCUSD", "BUY", 0.0006, info.getclosePrice().subtract(lower));*/
			double d = info.getclosePrice().subtract(lower).doubleValue();
			System.out.println(order.newOrder("BTCUSDT", "BUY", 0.0006, d));
			
		}
		
		
	}
	
	
	

	
	
	
	/** Actual trading program	testing*/
	/**
	 * This main is for testing purposes.
	 * 
	 * @param args
	 * @throws Exception
	 */	
	public static void main(String[] args) throws Exception {
		TradingStrategy lol = new TradingStrategy();
		while(true) {
		lol.strategy();	
		System.out.println(info.toString());
		Thread.sleep(10000);
		}
		
	}
	
}
